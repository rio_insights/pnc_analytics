import os

BASE_PATH = os.path.dirname(os.path.abspath(__file__))
TEMPLATE_PATH = os.path.join(BASE_PATH, "templates")
STATIC_PATH = os.path.join(BASE_PATH, "static")
LOG_PATH = os.path.join(BASE_PATH, "logs")

if not os.path.exists(LOG_PATH):
    os.makedirs(LOG_PATH)
