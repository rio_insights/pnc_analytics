from tornado.web import RequestHandler

from ..log.logger_factory import LoggerFactory

logger = LoggerFactory.get_logger(__name__)


class HomeHandler(RequestHandler):
    def get(self):
        logger.info("Received request in HomeHandler. Gonna process it now")
        self.render("index.html")

