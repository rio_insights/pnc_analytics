import asyncio
import ujson

from tornado import concurrent
from tornado.web import RequestHandler

from ..connectors.db import get_transactions_for_cnpj
from ..controllers.confidence import get_confidence
from ..log.logger_factory import LoggerFactory

executor = concurrent.futures.ThreadPoolExecutor(8)
logger = LoggerFactory.get_logger(__name__)


class ConfidenceHandler(RequestHandler):
    async def get(self):
        logger.info("Received request in /confidence. Gonna process it now")

        cnpj = self.get_argument("cnpjOrCpf")
        value = float(self.get_argument("value"))

        args = (cnpj, value)
        response = await asyncio.wrap_future(executor.submit(self.async_worker, *args))

        self.write(ujson.dumps(response))
        self.finish()

    def async_worker(self, cnpj, value):
        transactions = get_transactions_for_cnpj(cnpj)

        confidence = get_confidence(transactions, value)
        response = {"confidence": confidence}

        return response
