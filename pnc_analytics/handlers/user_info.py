import asyncio
import ujson

from tornado import concurrent
from tornado.web import RequestHandler

from ..connectors.db import get_user_by_cpf_or_cnpj
from ..log.logger_factory import LoggerFactory

executor = concurrent.futures.ThreadPoolExecutor(8)
logger = LoggerFactory.get_logger(__name__)


class UserInfoHandler(RequestHandler):
    async def get(self):
        logger.info("Received request in /user. Gonna process it now")

        type_ = self.get_argument("type")
        cnpj_or_cpf = self.get_argument("cnpjOrCpf")
        phone = self.get_argument("phone")

        args = (type_, cnpj_or_cpf, phone)
        response = await asyncio.wrap_future(executor.submit(self.async_worker, *args))

        self.write(ujson.dumps(response))
        self.finish()

    def async_worker(self, type_, cnpj_or_cpf, phone):
        user = get_user_by_cpf_or_cnpj(cnpj_or_cpf)

        if len(user) != 1:
            response = {"status": 200, "msg": "User does not exists"}
            return response

        user = user.iloc[0]
        if str(user["phone"]) != str(phone):
            response = {"status": 200, "msg": "Phone does not match"}
            return response

        response = {
            "name": user["name"],
            "cnpj": user["cnpj"],
            "phone": user["phone"],
        }

        return response
