import os
import sys
import logging

from logging import StreamHandler
from logging.handlers import RotatingFileHandler
from multiprocessing import current_process

from ..settings import LOG_PATH

LOG_FILENAME = "rio-{}.log"
loggers = {}


class LoggerFactory:
    @classmethod
    def __create_logger(cls, alias, filename=None):
        logger = logging.getLogger(alias)

        formatter = logging.Formatter('%(asctime)s %(levelname)s %(name)s %(message)s')

        file_handler = RotatingFileHandler(filename=filename, maxBytes=7340032,
                                           backupCount=4, encoding=None, delay=False)
        file_handler.setFormatter(formatter)

        stream_handler = StreamHandler(stream=sys.stdout)
        stream_handler.setFormatter(formatter)

        logger.addHandler(file_handler)
        logger.addHandler(stream_handler)
        logger.setLevel(logging.DEBUG)

        return logger

    @classmethod
    def get_logger(cls, alias):
        global loggers
        logger = loggers.get(alias)

        if not logger:
            process_name = current_process().name
            log_file = os.path.join(LOG_PATH, LOG_FILENAME.format(process_name))
            logger = cls.__create_logger(alias, log_file)
            loggers[alias] = logger

        return logger
