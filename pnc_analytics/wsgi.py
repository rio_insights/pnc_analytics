from tornado.web import Application
from tornado.ioloop import IOLoop

from .handlers.base import HomeHandler
from .handlers.user_info import UserInfoHandler
from .handlers.confidence import ConfidenceHandler
from .log.logger_factory import LoggerFactory
from .settings import TEMPLATE_PATH, STATIC_PATH

logger = LoggerFactory.get_logger(__name__)


def make_app():
    handlers = [
        (r"/", HomeHandler),
        (r"/confidence", ConfidenceHandler),
        (r"/user", UserInfoHandler)
    ]

    settings = {
        "template_path": TEMPLATE_PATH,
        "static_path": STATIC_PATH,
    }

    return Application(handlers, **settings)


def serve_forever():
    app = make_app()
    app.listen(80)

    logger.info("Launching RIO")

    IOLoop.current().start()