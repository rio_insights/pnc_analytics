import ujson
import pandas as pd

from urllib.request import urlopen, Request


def get_transactions_for_cnpj(cnpj):
    request = Request("https://clever-puma-44.localtunnel.me")

    response = urlopen(request)
    response_json = ujson.loads(response.read())

    transactions = response_json["data"]["transactions"]

    data = []
    for d in transactions:
        if d["cnpjBuyer"] != cnpj:
            continue

        data_row = (
            float(d["transactionValue"]), 1 if d["transactionPaymentStatus"] else 0
        )
        data.append(data_row)

    df = pd.DataFrame(data=data, columns=["value", "paid"])

    return df
