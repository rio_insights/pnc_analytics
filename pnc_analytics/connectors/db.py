import os
import pandas as pd


def get_users_table():
    path = os.path.join(os.path.dirname(__file__), "data", "users.csv")
    table = pd.read_csv(path, sep=",")
    table.cnpj = table.cnpj.apply(str)
    table.phone = table.phone.apply(str)

    return table


def get_transactions_table():
    path = os.path.join(os.path.dirname(__file__), "data", "transactions.csv")
    table = pd.read_csv(path, sep=",")
    table.cnpjBuyer = table.cnpjBuyer.apply(str)
    table.cnpjVendor = table.cnpjVendor.apply(str)

    return table


def get_transactions_for_cnpj(cnpj):
    table = get_transactions_table()
    cnpj_table = table[table.cnpjBuyer == cnpj]

    return cnpj_table[["transactionValue", "transactionPaymentStatus"]]


def get_user_by_cpf_or_cnpj(cpf_or_cnpj):
    table = get_users_table()

    user = table[table.cnpj == cpf_or_cnpj]

    return user
