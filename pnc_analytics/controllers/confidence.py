import numpy as np

from scipy.stats import beta


def get_confidence(transactions, value):
    a_prior = 10
    b_prior = 10

    paid = sum(transactions[transactions.transactionPaymentStatus == 1].transactionValue)
    not_paid = sum(transactions[transactions.transactionPaymentStatus == 0].transactionValue)

    a = paid + a_prior
    b = not_paid + b_prior

    rv = beta(a, b)

    confidence = 0.0
    for x in np.arange(0.0, 1.0, 0.001):
        if rv.cdf(x) > 0.2:
            confidence = x
            break

    decay = min(1.0, (paid - not_paid) / (2 * value))

    return confidence * decay
