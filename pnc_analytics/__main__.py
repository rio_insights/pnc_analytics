import multiprocessing

from .wsgi import serve_forever

WEB_PROCESS_NAME = "web"
TERMINATE_TIMEOUT = 5
PROCESSES = []


def main():
    process = multiprocessing.Process(
        target=serve_forever,
        name=WEB_PROCESS_NAME,
    )

    process.start()
    PROCESSES.append(process)


if __name__ == "__main__":
    try:
        main()
    except (SystemExit, KeyboardInterrupt, AssertionError):
        for process in PROCESSES:
            process.join(TERMINATE_TIMEOUT)

        raise

